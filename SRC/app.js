const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const port = 3333;

require('dotenv').config()
var mongoose = require('mongoose');
const cors = require('cors');
const cookieParser = require('cookie-parser')
    //facebook


// Import routes
const indexUser = require("./routes/index")
const facebook = require('./routes/facebookLogin')
const googleUser = require("./routes/google")
const quiz = require('./routes/createQuizs')
const category = require('./routes/category')
const ques = require('./routes/question')
const managePro = require('./routes/manageProfile')
const library = require('./routes/library')
    // const { registerValidation, loginValidation } = require("./auth/validation")
    // const bcrypt = require("bcryptjs")
    // Gửi yêu cầu phân tích kiểu nội dung application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// Gửi yêu cầu phân tích kiểu nội dung application/json để khỏi gưit app/json trogn header
app.use(bodyParser.json())
app.use(cookieParser());
// Route middlewares
app.set("view engine", "ejs")
app.use(cors())

//protect route
const ProtectedRoutes = require('./routes/protectedRoutes');

app.use('/api', ProtectedRoutes);

//not to protect
app.use('/user', indexUser) // login + regis
app.use('/facebookLogin', facebook);
app.use('/authenticate', googleUser)

// need protect
ProtectedRoutes.use('/quiz', quiz);
ProtectedRoutes.use('/manage-profile', managePro);
ProtectedRoutes.use('/category', category)
ProtectedRoutes.use('/library', library)
ProtectedRoutes.use('/question', ques)
    // Lắng nghe các requests
app.listen(process.env.PORT || port, function() {
        console.log("Server listening port", +port)
    })
    //ghi db
app.use('/protected/', ProtectedRoutes);
// const verify = require('./auth/checkToken')
// app.use('/api', verify);
//middleware
// verify.use('/api/user', indexUser)

app.use('/user', indexUser)

ProtectedRoutes.use('/facebookLogin', facebook);
ProtectedRoutes.use('/authenticate', googleUser)
ProtectedRoutes.use('/quiz', quiz);
ProtectedRoutes.use('/manage-profile', managePro);
ProtectedRoutes.use('/category', category)

// Lắng nghe các requests



mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true }).then(function() {
    console.log("Successfully connected to the database");
}).catch(function(err) {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});