var express = require('express');
var mongo = require('mongodb');
var router = express.Router();
const category = require('../models/category')

router.post('/', async (req, res) => {
    try {
        const check = await category.findOne({ categoryName: req.body.categoryName });
        if (check)
            return res.status(400).send("Category name is already used");
        const temp = new category();
        if (req.body.parentID != null)
            temp.parentID = req.body.parentID;
        temp.categoryName = req.body.categoryName;
        const tmp = await temp.save();
        return res.status(201).send({
            _id: temp._id,
            parentID: temp.parentID,
            categoryName: temp.categoryName
        });
    } catch (err) {
        return res.status(400).send(err);
    }
});

router.patch('/one/:categoryName', async (req, res) => {
    try {
        const categoryName = req.params.categoryName;
        const update = req.body;
        const option = { new: true };

        const result = await user.findByIdAndUpdate(categoryName, update, option);
        if (result)
            return res.status(200).send({
                _id: result._id,
                parentID: result.parentID,
                categoryName: result.categoryName
            });
        return res.status(404).send("Category does not exist");
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.get('/one/:categoryName', async (req, res) => {
    try {
        const check = await category.findOne({ categoryName: req.params.categoryName });
        if (!check)
            return res.status(404).send("Category name does not exist");
        return res.status(200).send({
            _id: check._id,
            parentID: check.parentID,
            categoryName: check.categoryName
        });
    } catch (error) {
        return res.status(400).send(error);
    }
})

router.get('/one/:categoryName/subcategory', async (req, res) => {
    try {
        const cate = await category.findOne({ categoryName: req.params.categoryName })
        if (!cate) {
            return res.status(404).send("Category name does not exist");
        }

        const subCate = await category.find({ parentID: cate._id })
        if (!subCate.length) {
            return res.status(404).send("Category does not have any children");
        }
        return res.status(200).send(subCate);
    } catch (err) {
        return res.status(400).send(err);
    }
})

async function deleteCategory(id) {
    const tempCat = await category.find({ parentID: id })
    tempCat.forEach(function (element) {
        deleteCategory(element._id);
    })
    var temp = await category.findOneAndDelete({ _id: id });
}

router.delete('/one/:categoryName', async (req, res) => {
    try {
        const check = await category.findOne({ categoryName: req.params.categoryName })
        if (!check)
            return res.status(404).send("Category does not exist")
        deleteCategory(check._id);
        return res.status(200).send({
            _id: check._id,
            parentID: check.parentID,
            categoryName: check.categoryName
        });
    } catch (err) {
        return res.status(400).send(err);
    }
})

router.get('/allroots', async (req, res) => {
    try {
        const temp = await category.find({ parentID: null })
        return res.status(200).send(temp)
    }
    catch (err) {
        return res.status(400).send(err)
    }
})

router.get('/all', async (req, res) => {
    try {
        const temp = await category.find({}).sort({categoryName: 1})
        return res.status(200).send(temp)
    } catch (err) {
        return res.status(400).send(err)
    }
})

module.exports = router