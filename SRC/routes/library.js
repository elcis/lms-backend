var express = require('express')
var router = express.Router();
const quiz = require('../models/quiz')
const ques = require('../models/question')

//get all quiz
router.get('/quiz', async(req, res) => {
    try {
        const check = await quiz.find();

        if (!check)
            return res.status(404).send("can't load all quiz");
        return res.status(200).send(check);
    } catch (error) {
        return res.status(400).send(error);
    }
})

//
router.get('/question', async(req, res) => {
    try {
        const check = await ques.find();
        if (!check)
            return res.status(404).send("can't load all question");
        return res.status(200).send(check);
    } catch (error) {
        return res.status(400).send(error);
    }
})


module.exports = router;