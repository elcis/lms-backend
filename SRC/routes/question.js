var express = require('express');
var router = express.Router();
const ques = require('../models/question')


router.post('/manage-question/', async(req, res) => {
    const question = new ques()
    question.userID = req.body.userID
    question.category=req.body.category
    question.question_list = req.body.question_list
    try {
        const ques1 = await question.save()
        res.status(200).send(ques1)
    } catch (err) {
        res.status(400).send(err)
        console.log('cant create quesiton') // bad request
    }
})
router.get('/manage-user/:userID', async(req, res) => {
    try {
        const check = await ques.find({ userID: req.params.userID });
        if (!check)
            return res.status(400).send("User not exist");
        return res.status(200).send(check);
    } catch (error) {
        return res.status(400).send(error);
    }
})
router.get('/manage-question/:_id', async(req, res) => {
    try {
        const check = await ques.find({ _id: req.params._id });
        if (!check)
            return res.status(400).send("ques not exist");
        console.log(check)
        return res.status(200).send(check);
    } catch (error) {
        console.log(error)
        return res.status(400).send(error);
    }
})
router.patch('/manage-question/:_id', async(req, res) => {
    const options = { useFindAndModify: false }
    try {
        const id = req.params._id
        
        const result = await ques.findOneAndUpdate({ _id: id }, { question_list: req.body.question_list }, options);
        console.log(result)
        res.status(200).send("ok");
    } catch (error) {
        console.log(error)
        res.status(400).send(error);
    }

});
router.delete('/manage-question/:_ID', async(req, res) => {
    try {
        const check = await ques.deleteOne({ _id: req.params._ID });
        if (!check)
            return res.status(400).send("ques not exist");
        console.log(check)
        return res.status(200).send("ok");
    } catch (error) {
        console.log(error)
        return res.status(400).send(error);
    }
})
module.exports = router