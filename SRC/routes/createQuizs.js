var express = require('express')
var router = express.Router();
const quiz = require('../models/quiz') // ==> model quiz
const score = require('../models/scores')
const user = require('../models/user') // ==> xác định ai là chủ sở hữu
router.get('/manage-quiz/:name', async(req, res) => {

        try {
            const check = await quiz.findOne({ name: req.params.name });
            if (!check)
                return res.status(400).send("Quiz name does not exist");
            return res.status(200).send(check);
        } catch (error) {
            return res.status(400).send(error);
        }
    }) //=> routr for quiz OK




router.post('/manage-quiz', (req, res) => { // OK

    const newquiz = new quiz();
    newquiz.id = String(newquiz._id);
    newquiz.name = req.body.name;
    newquiz.category = req.body.category;
    newquiz.avatar = req.body.avatar;
    newquiz.privacy = req.body.privacy;
    newquiz.time = new Date();
    newquiz.author = req.body.author; // save id of user 


    const newscore = new score();
    newscore.id_quiz = newquiz.id
    newscore.scores = null;
    try {
        const Quiz = newquiz.save();

        const Score = newscore.save(); // luu table score

        res.status(200).send(newquiz); // ok 
    } catch (err) {
        res.status(400).send(err)
        console.log('cant create quiz') // bad request
    }

})

//update quiz and submit again
router.patch('/manage-quiz/:_id', async(req, res) => {

    try {
        const _id = req.params._id;
        req.body.time = new Date();
        const update = req.body
        const option = { new: true };
        const result = await quiz.findOneAndUpdate(_id, update, option);

        res.status(200).send(result);
    } catch (error) {
        res.status(400).send(error);
    }

});

//error 404 not sevrer
// router.delete('/delete/:_id', async(req, res) => {

//     try {


//         const option = { new: true };
//         const del = await quiz.findOneAndDelete(req.params._id); // find in dtb quiz ang delete

//         if (!del) return res.sendStatus(404);
//         return res.send(del);
//     } catch (error) {
//         return res.sendStatus(400);
//     }
// })

module.exports = router;