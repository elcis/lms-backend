var express = require('express');
var router = express.Router();
const user = require('../models/user')

router.get('/:username', async(req, res) => {
    const profile = await user.findOne({
        username: req.params.username
    });
    if (!profile)
        return res.status(400).send("Không tìm thấy người dùng");
    return res.status(200).send({
        email: profile.email,
        name: profile.name,
        avatar: profile.avatar,
        username: profile.username,
        birthyear: profile.birthyear,
        type: profile.type
    });
})

router.patch('/:username', async(req, res) => {
    try {
        const username = req.params.username;
        const update = req.body;
        const option = { new: true };
        const result = await user.findByIdAndUpdate(username, update, option);
        if (result)
            res.status(200).send(result);
        return res.status(400).send("Username does not exist");
    } catch (error) {
        res.status(400).send(error);
    }
})

module.exports = router