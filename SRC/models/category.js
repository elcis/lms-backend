const mongoose = require("mongoose")

const categorySchema = new mongoose.Schema({
    parentID: { type: mongoose.SchemaTypes.ObjectId },
    categoryName: { required: true, type: String },
}, { collection: 'categories' });

module.exports = mongoose.model('category', categorySchema);