const mongoose = require("mongoose")

const scoreSchema = new mongoose.Schema({
    id_quiz: String,
    scores: [{
        id_user: String,
        score: String
    }]
}, { collection: 'scores' });

module.exports = mongoose.model('scores', scoreSchema)