const mongoose = require("mongoose")

const question_list = new mongoose.Schema({
    userID: { required: true, type: String},
    category:{required:true,type:String},
    question_list:[{ 
        ID: { required: true, type: String},
        type: { required: true, type: String },
        time:{required:true,type:String},
        image:{required: false,tupe:String},
        question: { required: true, type: String },
        answer: { required: true, type: [String], default: null },
        correct_answer:{required: true, type: [String]},
        description:{reqired:false,type:String}
    }],
},{ collection: 'questions' });

module.exports = mongoose.model('question', question_list);